# 2nde - Enseignement commun de seconde générale et technologique

* [Comment les économistes, les sociologues et les politistes raisonnent-ils et travaillent-ils ?](epistemologie.md)

* Science économique
    * [Comment crée-t-on des richesses et comment les mesure-t-on ?](economie/production.md)
    * [Comment se forment les prix sur un marché ?](economie/marche.md)
* Sociologie et science politique
    * [Comment devenons-nous des acteurs sociaux ?](sociologie/socialisation.md)
    * [Comment s'organise la vie politique ?](sociologie/vie-politique.md)
* Regards croisés
    * [Quelles relations entre le diplôme, l'emploi et le salaire ?](regards-croises/diplomes.md)
