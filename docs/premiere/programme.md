# Enseignement de spécialité de première générale

* Science économique
    * [Comment un marché concurrentiel fonctionne-t-il ?](economie/marche.md)
    * [Comment les marchés imparfaitement concurrentiels fonctionnent-ils ?](economie/marche-imparfait.md)
    * [Quelles sont les principales défaillances du marché ?](economie/defaillance-marche.md)
    * [Comment les agents économiques se financent-ils ?](economie/financement.md)
    * [Qu'est-ce que la monnaie et comment est-elle créée ?](economie/monnaie.md)
* Sociologie et sciences politiques
    * [Comment la socialisation contribue-t-elle à expliquer les différences de comportement des individus ?](sociologie/socialisation.md)
    * [Comment se construisent et évoluent les liens sociaux ?](sociologie/lien-social.md)
    * [Quels sont les processus sociaux qui contribuent à la déviance ?](sociologie/deviance.md)
    * [Comment se forme et s'exprime l'opinion publique ?](sociologie/opinion.md)
    * [Voter : une affaire individuelle ou collective ?](sociologie/vote.md)
* Regards croisés
    * [Comment l'assurance et la protection sociale contribuent-elles à la gestion des risques dans les sociétés développées ?](regards-croises/risque.md)
    * [Comment les entreprises sont-elles organisées et gouvernées ?](regards-croises/entreprise.md)