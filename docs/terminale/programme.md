# Enseignement de spécialité de terminale générale

* Science économique
    * [Quels sont les sources et les défis de la croissance économique ?](economie/croissance.md)
    * [Quels sont les fondements du commerce international et de l'internationalisation de la production ?](economie/commerce-international.md)
    * [Comment lutter contre le chômage ?](economie/chomage.md)
    * [Comment expliquer les crises financières et réguler le système financier ?](economie/crises-financieres.md)
    * [Quelles politiques économiques dans le cadre européen ?](economie/europe.md)
* Sociologie et science politique
    * [Comment est structurée la société française actuelle ?](sociologie/stratification.md)
    * [Quelle est l'action de l'École sur les destins individuels et sur l'évolution de la société ?](sociologie/ecole.md)
    * [Quels sont les caractéristiques contemporaines et les facteurs de la mobilité sociale ?](sociologie/mobilite-sociale.md)
    * [Quelles mutations du travail et de l'emploi ?](sociologie/mutations-travail.md)
    * [Comment expliquer l'engagement politique dans les sociétés démocratiques ?](sociologie/engagement-politique.md)
* Regards croisés
    * [Quelles inégalités sont compatibles avec les différentes conceptions de la justice sociale ?](regards-croises/inegalites-justice-sociale.md)
    * [Quelle action publique pour l'environnement ?](regards-croises/action-publique-environnement.md)
