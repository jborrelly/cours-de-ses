# Quels sont les sources et les défis de la croissance économique ?

## Cours

### **Introduction** - Pourquoi s’intéresser à la croissance économique ?

* [Écouter "Respire 2020" par mickey3d et Bigflo & Oli (P&C 2020 Warner Music France, Label Parlophone)](https://youtu.be/3OhqqT49VS0?feature=shared)
* [ARTE - Avons-nous besoin de la croissance économique ? | 42, la réponse à presque tout - 2022](https://youtu.be/p8VUPqp-97s?feature=shared)
* [Greenpeace France : Tout ça pour des nuggets ? - 2021](https://www.youtube.com/watch?v=DkWzoXJsBM4)

!!! quote "Citation"

    L’économie est la science qui étudie comment les ressources rares sont employées pour la satisfaction des besoins des hommes vivant en société ; elle s’intéresse d’une part, aux opérations essentielles que sont la production, la distribution, et la consommation des biens, d’autre part, aux institutions et aux activités ayant pour objet de faciliter ces opérations.
    
    &mdash; Edmond MALINVAUD, *Leçons de théorie microéconomique*, 1986

### **I. Qu’est-ce que la croissance économique ?**

!!! abstract "Définition"

    La **croissance économique** désigne l’augmentation de la production sur une longue période. On utilise en général le taux de variation du PIB en volume pour la mesurer.

#### A. De la construction du PIB à la mesure de la croissance économique

* [Dessine-moi l’éco - Qu’est-ce que le Produit Intérieur Brut (PIB) ?](https://www.youtube.com/watch?v=ROpFSrUMs-A&t=1s)
* [INSEE - Qu’est ce que le PIB ?](https://www.youtube-nocookie.com/embed/zwyvFHomT_A)
* [France Info - Expliquez-nous... la croissance](https://www.dailymotion.com/video/x2q6m8g)
* [SOS SES - Episode 1 : "Le PIB et la croissance économique, c’est quoi ?"](https://www.youtube-nocookie.com/embed/f24QGF2vG_U)
* [GDP Calculation Method - This video goes over everything you need to know about GDP. This video covers how to find the GDP, what is included in the GDP, and what is not part of the GDP.](https://www.youtube-nocookie.com/embed/eMzKpc_ASTA)

Points clés :

* La croissance économique repose sur un indicateur de production.
* La croissance économique un phénomène quantitatif et de long terme que l’on peut mesurer.

#### B. L’intérêt du PIB

* Le PIB est un instrument de mesure de la richesse d’un pays.

* Le PIB  est un outil qui permet des comparaisons dans le temps et dans l’espace, entre la taille et la puissance relative des économies nationales à travers le monde.

* Le PIB relativise le poids de certaines variables dans une économie. Il s’agit d’une mesure qui permet de relativiser certains phénomènes dans le temps ou l’espace.

#### C. Les limites du PIB

[Data Gueule - France Télévisions  Le PIB, cette fausse boussole](https://youtu.be/4-V4SFp5S-k?feature=shared)

* Les limites méthodologiques de calcul du PIB.

* Le PIB ne mesure qu’une partie de l’activité économique et des richesses produites.

* Le PIB ne prend pas bien en compte le développement et le bien-être.

#### D. D’autres indicateurs permettent de combler certaines de ses lacunes

### **II. Quelles sont les sources (ou facteurs) de la croissance économique ?**

* [Hatier - Les origines de la croissance économique](https://www.youtube-nocookie.com/embed/EEFrSeNesOo)
* [L’Antisèche -  Sources de la croissance économique](https://www.youtube-nocookie.com/embed/k_JDaSm7D00)
* [Les sources de la croissance - Philippe Aghion](https://www.youtube-nocookie.com/embed/gJ2fvp7JLvw)

#### A. Aux origines de la croissance : augmenter les facteurs de production (croissance extensive)

#### B. Améliorer l’efficacité, Accroissement de la productivité globale des facteurs (le rôle du progrès technique dans la croissance intensive)

#### C. Théories de la croissance endogène : accumulation du capital, économies d’échelles et externalités positives expliquent la croissance économique

* [Sens éco : La croissance endogène](https://www.youtube-nocookie.com/embed/XiffQrRWfi0)
* [SOS SES - La croissance endogène : 1 notion, en 2 minutes!](https://www.youtube-nocookie.com/embed/gNuaH6E7lyE)

#### D. Le rôle des institutions dans la croissance économique

### **III. Quels sont les effets de la croissance économique et du progrès technique ?**

#### A. Quels sont les effets et les conséquences du Progrès technique ?

#### B. La croissance peut-elle être respectueuse de l’environnement ? Quelles sont les limites écologiques de la croissance ?

* [Débat : Le développement économique et la protection de l’environnement sont ils compatibles ?](https://www.youtube-nocookie.com/embed/wE3kKY0HGaM)
* [Pouvons-nous faire décroître l’économie ? | 42 - La réponse à presque tout | ARTE](https://www.youtube-nocookie.com/embed/URn334bS-tw)

## **Évaluation**

### *Dissertations*

!!! tip "Astuce"

    Retrouvez la méthode pour réussir sa dissertation dans la Rubrique **Méthodologie**

* **Sujets portant sur les sources de la croissance économique**

L’augmentation des facteurs de productions, travail et capital, est-elle la seule source de la croissance économique ? L’accumulation des facteurs de production est-elle la seule  source de croissance économique ? L’accroissement de la productivité globale des facteurs suffit-il à expliquer la croissance économique ? Quelles sont les sources de la croissance
économique ? Le progrès technique est-il toujours source de croissance ? Les facteurs travail et capital sont-ils suffisants pour
expliquer la croissance ? Quel est le rôle du progrès technique dans le processus de croissance économique ? Comment le
progrès technique favorise-t-il la croissance économique ?

* **Sujets sur les effets de la croissance économique et du progrès technique**

Le progrès technique rend-il la croissance durable ? Le progrès technique n’a-t-il que des effets positifs ? Comment l’innovation
peut-elle contribuer à reculer les limites écologiques d’une croissance soutenable ?

!!! tip "Astuce"

    Étudiez les plans des cours pour vous aider à construire un développement.

### *Mobilisation de connaissances*

* **Sujets autour de la Croissance économique et PIB**

Distinguez la croissance intensive de la croissance extensive. Quels sont les intérêts du PIB ? Quelles sont les limites du PIB ?

* **Sujets autour des sources (ou des facteurs) de la croissance économique**

Illustrez par un exemple chacune des sources de la croissance économique. Montrez que la croissance économique n’est pas que le résultat de
l’augmentation des facteurs de production utilisés. Présentez le rôle de la quantité et de la qualité des facteurs de production dans la croissance économique. Comment l’accumulation du capital est-elle source de croissance ? A l’aide d’un exemple, décrivez en quoi la croissance est un phénomène cumulatif. Montrez que la productivité globale des facteurs est source de croissance économique. Présentez le lien entre productivité globale des facteurs et progrès technique. Montrez que le progrès technique est endogène. Pourquoi la hausse de la productivité globale des facteurs-est-elle source de croissance ? Quelles sont les caractéristiques du progrès technique ? A l’aide d’un exemple,
montrez que la productivité globale des facteurs est source de croissance. Comment les brevets peuvent-ils favoriser la croissance ? Comment les droits de propriété favorisent-ils la croissance économique ? À partir de l’exemple des droits de propriété, montrez comment les institutions influent sur la croissance économique. Vous montrerez comment les droits de propriété peuvent favoriser la croissance économique. Quelles sont les caractéristiques des institutions qui favorisent la croissance ? Quel est le rôle des institutions dans le processus de croissance ? A l’aide d’exemples, montrez comment les institutions peuvent être facteur de croissance économique.

* **Sujets autour des effets de la croissance économique et du progrès technique**

Montrez que la croissance économique engendre des externalités positives et négatives. Présentez un mécanisme par lequel le progrès
technique peut engendrer des inégalités de revenus. Montrez que le progrès technique peut engendrer des inégalités de revenus. Comment le
progrès technique peut-il engendre des inégalités de revenus ? Montrez comment la destruction créatrice peut produire des inégalités de
revenus. Expliquez quel peut être l’effet du progrès technique sur les revenus des plus qualifiés. Présentez à travers un exemple le processus
de destruction créatrice. À l’aide d’un exemple, vous montrerez que l’innovation s’accompagne d’un processus de destruction créatrice. À
partir d’un exemple, vous montrerez que l’innovation peut aider à reculer les limites écologiques de la croissance. Illustrez par deux exemples
l’idée que le progrès technique peut être une solution pour relever le défi de la croissance soutenable. Montrez que la croissance économique
se heurte à des limites écologiques. Comment l’innovation peut-elle aider à faire reculer les limites écologiques de la croissance ? À l’aide d’un
exemple, vous montrerez que la croissance économique se heurte à des limites écologiques. Quelles sont les limites écologiques qui peuvent
remettre en cause la soutenabilité de la croissance ? Montrez que l’innovation peut aider à reculer les limites écologiques auxquelles se heurte
la croissance économique.

### **Étude de document**

### **Raisonnement s’appuyant sur un dossier documentaire**

* **Sujets sur les sources de la croissance économique**

Vous montrerez que le progrès technique est endogène. Vous présenterez les sources du progrès technique. Vous montrerez que les institutions jouent un rôle dans la croissance économique. Vous montrerez que les facteurs de production sont sources de croissance économique. Vous expliquerez le ralentissement des gains de productivité.

* **Sujets sur le rôle du progrès techniques**

vous montrerez que l’innovation peut aider à reculer les limites écologiques de la croissance. vous montrerez que le progrès technique a des effets contradictoires sur l’activité économiques. Vous montrerez que le progrès technique peut engendrer des inégalités de revenus. vous expliquerez le lien entre progrès technique et croissance.

* **Sujets sur les limites écologiques de la croissance économique**

Vous montrerez que la croissance économique contemporaine se heurte à des limites environnementales et sociales. Vous montrerez comment l’innovation peut être une solution aux limites écologiques de la croissance économique. Vous montrerez comment l’innovation peut contribuer à la soutenabilité de la croissance économique.

## **Ressources**

!!! note "Références"

    Comment noter une référence dans sa copie ?

    nom auteur autrice, ^^titre souligné^^, date.
