function fix_links() {
    for(let links = document.getElementsByTagName("a"), i = 0; i < links.length; i++) {
        let link = links[i];

        // Open all external links in a new tab with a11y attributes
        if(link.getAttribute("href") && link.hostname !== location.hostname) {
            link.target = "_blank";
            link.rel = "noreferrer";
            link.title = link.hostname + " - Ouvre une nouvelle fenêtre";
        }

        // Fix permalink titles
        if(link.classList.contains('headerlink')) {
            link.title = "Lien permanent";
        }
    }
}

if (typeof document$ !== "undefined") {
    document$.subscribe(function() {
        fix_links();
    })
}