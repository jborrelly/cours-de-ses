# Cours de Sciences économiques et sociales

Site de cours de Sciences économiques et sociales de Julien Borrelly. Tous niveaux de la Seconde au Supérieur. Les contenus seront ajoutés progressivement au cours de l'année scolaire 2024/2025.

Ce site a été généré à partir du [modèle de site web de cours général](https://forge.apps.education.fr/docs/modeles/site-web-cours-general) de la forge des communs numériques éducatifs.

## Installer le site sur un poste de travail

Prérequis :

* installer les logiciels git, python et pip
* configurer une clé SSH

Cloner le dépôt et se placer à sa racine :

```sh
git clone git@forge.apps.education.fr:jborrelly/cours-de-ses.git
cd cours-de-ses
```

Installer les dépendances (mkdocs et ses plugins) :

```sh
pip install -r requirements.txt
```

:warning: si vous venez d'installer Python sous Windows, il faut que le répertoire qui contient mkdocs soit dans le PATH.

Déployer en local

```sh
mkdocs serve
```

Le site est maintenant déployé à l'adresse `https://localhost:8000` et il se met à jour à chaque modification de fichier (contenu ou configuration).

## Mémo commandes Git

Récupérer les modifications de la forge :

```sh
git pull origin
```

Ajouter des fichiers à un lot de modifications (aka. *commit*) :

```sh
git add <nom de fichier ou de dossier ou . pour tout ajouter>
```

Enregistrer un lot de modifications :

```sh
git commit -m "Message décrivant le lot de modification"
```

Envoyer le lot de modification sur la forge :

```sh
git push origin
```
